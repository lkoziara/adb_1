# -*- coding: utf-8 -*-

import sys
import traceback
import time
import os
import re
import math
import threading
import datetime


class KeySight34972A:
    def __init__(self, device):
        self.device = device
        self.log_file = "temperature_log.log"
        self.log_cycle = 60          # Czas co jaki następuje zapis parametrów
        self.capture_values = True

    def getName(self):
        self.write("*IDN?")
        return self.read(300)

    def measureTemperature(self, sockets=[], length=4000):
        '''measure temperature
        '''
        if len(sockets) == 0:
            return None
        cmd = "MEAS:TEMP? (@%s)" % \
            ','.join([str(st) for st in sockets])
        usb = os.open(self.device, os.O_RDWR)
        os.write(usb, cmd)
        time.sleep(0.2)
        t = os.read(usb, length)
        os.close(usb)
        return t

    def continuousMeasureTemperature(self, sockets=[], log_file=None):
        self.capture_values = True
        if log_file is not None:
            self.log_file = log_file

        def measure():
            while self.capture_values:
                try:
                    temp = self.measureTemperature(sockets)
                    line = ""
                    for m in temp.split(','):
                        line += ''.join(re.findall(r'^[-]', m))
                        p = ''.join(re.findall(r'E([-+]\d+)', m))
                        line += str(float(''.join(re.findall(r'\d+.\d', m)))*pow(10, float(p)))
                        line += "; "
                    with open(self.log_file, "a") as myfile:
                        myfile.write("%s %s\n" % (line, datetime.datetime.now()))
                except:
                    print "Capturing Error", traceback.format_exc()
                finally:
                    time.sleep(self.log_cycle)
        d = threading.Thread(name='temperature_logs', target=measure)
        d.setDaemon(True)
        d.start()

    def stopContinousMeasure(self):
        self.capture_values = False

    def configTemperature(self, type="K", sockets=[]):
        '''temperature measurement using thermocouples
          type - type of thermocouple
          socket - ports to configuration
        '''
        if len(sockets) == 0:
            return None
        cmd = "CONF:TEMP TC, %s, (@%s)" % \
            (type, ','.join([str(st) for st in sockets]))
        usb = os.open(self.device, os.O_WRONLY)
        os.write(usb, cmd)
        os.write(usb, "UNIT:TEMP C, (@%s)"
                 % ','.join([str(st) for st in sockets]))
        os.close(usb)

if __name__ == "__main__":
    import datetime

    key = KeySight34972A("/dev/usbtmc0")

    key.configTemperature(type="J", sockets=[101])
    time.sleep(1)
    key.configTemperature(type="K", sockets=[102, 103, 104, 105, 106, 107, 108, 109, 110, 111])
    time.sleep(1)
    print "measure"
    key.continuousMeasureTemperature(sockets=[102, 103, 104, 105, 106, 107, 108, 109, 110, 111])
    # print key.measureTemperature([102, 103, 104, 105, 106, 107, 108, 109, 110, 111])

    time.sleep(10*60)
    key.stopContinousMeasure()
