# -*- coding: utf-8 -*-


def CRC256(text):
    B = 0
    J = 0
    for ch in list(text):
        J = ord(ch)
        B = (B - J) % 256
    J = int(math.floor(B / 16))
    if J < 10:
        J += 48
    else:
        J += 55
    K = B % 16
    if K < 10:
        K += 48
    else:
        K += 55
    return chr(J)+chr(K)