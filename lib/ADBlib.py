# -*- coding: utf-8 -*-
import os
from sys import platform as _platform
import sys
import traceback
import urllib2
import time
import json
import re


def ping(hostname):
    if _platform == "linux" or _platform == "linux2":
        return os.system("ping -c 4 " + hostname)
    elif _platform == "win32":
        return os.system("ping " + hostname)


def check_tats(stb, filename):
    stb.send("tats 0")
    time.sleep(4)
    response = stb.get_response()
    if "SPECTRUM" in response:
        print "\tTATS - OK"
        with open(filename, "a") as myfile:
            tmp = '\n\t  '.join(response.split('\n')[1:])
            tmp = tmp.replace('\r', '')
            myfile.write("\t%s" % tmp)
            myfile.write("\n\tTATS 0 - OK")
        return True
    else:
        TEST_SUCCESS = False
        print "\tTATS - ERROR"
        with open(filename, "a") as myfile:
            tmp = '\t  '.join(response.split('\n')[1:])
            myfile.write("\t%s" % tmp)
            myfile.write("\n\tTATS 0 - ERROR")
        return False


def setDHCPandPING(stb, logfile):
    stb.send("netdhcp 2 1")
    time.sleep(10)
    stb.send("netstatus 2")
    time.sleep(2)
    response = stb.get_response()
    with open(logfile, "a") as myfile:
        myfile.write("\n\n\tNETSTATUS 2:\n")
        tmp = '\n\t  '.join(response.split('\n')[1:])
        myfile.write("\t%s" % tmp)
    if "boot.prot" in response:
        with open(logfile, "a") as myfile:
            myfile.write("\n\tNETSTATUS 2 - OK")
    else:
        with open(logfile, "a") as myfile:
            myfile.write("\n\tNETSTATUS 2 - ERROR")

    hostname = []
    try:
        hostname = re.findall(r'ip:\s+(192.168.\d+.\d+)', response)
    except:
        print "\tERRR - hostname"
        hostname = ["192.168.1.100"]
    # ######################## PING ###################### 4s
    if hostname[0] != "192.168.10.100":
        response = ping(hostname[0])
        if response == 0:
            print "\tPING", hostname[0], " - OK"
            with open(logfile, "a") as myfile:
                myfile.write("\n\tPING" + hostname[0] + " - OK")
            return True
        else:
            print "\tPING", hostname[0], " ERROR"
            with open(logfile, "a") as myfile:
                myfile.write("\n\tPING - ERROR")
            return False
    else:
        with open(logfile, "a") as myfile:
            myfile.write("\n\tPING - ERROR - not working dhcp")
            print "\tPING - ERROR - not working dhcp"
        return False


def RRsystem(ErrorListStart, ErrorListEnd, filename):
    result = True
    try:
        startLen = len(ErrorListStart)
        endLen = len(ErrorListEnd)
        alarms_log = None
        if startLen < endLen:
            alarms_log = {}
            for alarm in ErrorListEnd[startLen:]:
                if alarm["Mode"] in alarms_log:
                    alarms_log[alarm["Mode"]] += 1
                else:
                    alarms_log[alarm["Mode"]] = 1
            if alarms_log is not None:
                result = False
                print "\t", alarms_log
                with open(filename, "a") as myfile:
                    for key, item in alarms_log.iteritems():
                        myfile.write("\n\t%s: %s" % (key, item))
        else:
            return result
    except:
        result = False
        with open("script_error.log", "a") as myfile:
            myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
            myfile.write("\n%s\n" % traceback.format_exc())
    finally:
        return result
