# -*- coding: utf-8 -*-
u'''
Komunikacja RS232
'''
import serial
import Queue
from threading import Thread
import threading
import sys
import traceback
import datetime
import re


class COM():
    def __init__(self, baudrate=9600, com='/dev/ttyUSB0', timeout=1):
        #Thread.__init__(self)
        self.serial = serial.Serial()
        self.serial.baudrate = baudrate
        self.serial.port = com
        serial.timeout = timeout
        self._work_flag = True

        self.response = ""
        self.log_flag = False
        self.counter = 0
        self.qcmd = Queue.Queue()

    def run(self):

        while self._work_flag and not self.serial.isOpen():
            try:
                self.serial.open()
            except:
                traceback.print_exc(file=sys.stdout)
                return

        self.response = ""

        while self._work_flag:
            if not self.qcmd.empty():
                self.response = ""
                cmd = self.qcmd.get(False)
                self.serial.write(cmd+'\n')
            try:
                data = self.serial.readline()
                data = data.replace('\r\n', '')
                data = data.replace('\r', '')
                data = data.replace('\n', '')

                if "MEMTEST" in data:
                    self.response = ""
                    self.counter += 1
                    self.log_flag = True

                if self.log_flag:
                    out = data.replace('\b', '')

                    redata = re.sub(r'(\\\|/-)', "", out)
                    if redata != data:
                        out = re.sub(r'(\s+)', " ", redata)
                        out = re.sub(r'\|', "", out)

                    redata2 = re.sub(r'(setting+\s+\d+testing+\s+\d+)', "", out)
                    if redata2 != data:
                        out = re.sub(r'(\s+)', " ", redata2)

                    self.response += out
                    self.response += "\n"

                if ("BCM75840" in data) and (self.response != ""):
                    self.log_flag = False

                    with open("memory_test_log.log", "a") as myfile:
                        if "ERROR" in self.response:
                            myfile.write("TEST : "+str(self.counter)+" ERROR"+"\n")
                            print str(self.counter)+" ERROR"
                        else:
                            myfile.write("TEST : "+str(self.counter)+" OK"+"\n")
                            print str(self.counter)+" OK"
                        myfile.write(self.response+"\n\n")
            except:
                traceback.print_exc(file=sys.stdout)
                st.stop()

    def send(self, cmd):
        print u">", cmd
        self.qcmd.put(cmd)

    def clearQ(self):
        with self.qcmd.mutex:
            self.qcmd.queue.clear()

    def stop(self):
        self._work_flag = False
        self.serial.close()

if __name__ == "__main__":
    print u"Rozpoczecie testu", datetime.datetime.now()
    st = COM(115200, "/dev/ttyUSB2", 30)
    st.run()
