# -*- coding: utf-8 -*-
u'''
Komunikacja RS232
'''
import serial
import Queue
from threading import Thread
import threading
import sys
import traceback
import datetime
import re
from WKL64 import *
from mFi import *


class COM():
    def __init__(self, baudrate=115200, com='/dev/ttyUSB0', timeout=1):
        self.serial = serial.Serial()
        self.serial.baudrate = baudrate
        self.serial.port = com
        serial.timeout = timeout
        self._work_flag = True

        self.response = ""
        self.log_flag = False
        self.counter = 0
        self.qcmd = Queue.Queue()

        self.chamber = WKL64(19200, "/dev/ttyUSB0", bus=1)
        time.sleep(1)
        self.chamber.start()
        time.sleep(2)
        self.chamber.capture_logs()
        self.chamber.set_values(temperature=0, oStart=True)

        self.mFi = mFi("192.168.0.2", "ubnt", "ubnt")
        self.mFi.switchOFF(4)

        time.sleep(1)
        self.mFi.switchON(4)

        self.half = False
        self.all = False

    def run(self):

        while self._work_flag and not self.serial.isOpen():
            try:
                self.serial.open()
            except:
                traceback.print_exc(file=sys.stdout)
                return

        self.response = ""

        while self._work_flag:
            if not self.qcmd.empty():
                self.response = ""
                cmd = self.qcmd.get(False)
                self.serial.write(cmd+'\n')
            try:
                data = self.serial.readline()
                data = data.replace('\r\n', '')
                data = data.replace('\r', '')
                data = data.replace('\n', '')

                if "Start skryptu" in data:
                    self.response = ""
                    self.counter += 1
                    self.log_flag = True

                if self.log_flag:
                    out = data.replace('\b', '')

                    redata = re.sub(r'(\\\|/-)', "", out)
                    if redata != data:
                        out = re.sub(r'(\s+)', " ", redata)
                        out = re.sub(r'\|', "", out)

                    redata2 = re.sub(r'(setting+\s+\d+testing+\s+\d+)', "", out)
                    if redata2 != data:
                        out = re.sub(r'(\s+)', " ", redata2)

                    self.response += out
                    self.response += "\n"

                if "Cykl testu" in data:
                    self.log_flag = False

                    with open("flash_test_log.log", "a") as myfile:
                        if "negatywny" in self.response:
                            myfile.write("TEST : "+str(self.counter)+" ERROR "+str(datetime.datetime.now())+"\n")
                            print str(self.counter)+" ERROR  "+str(datetime.datetime.now())
                        else:
                            myfile.write("TEST : "+str(self.counter)+" OK "+str(datetime.datetime.now())+"\n")
                            print str(self.counter)+" OK  "+str(datetime.datetime.now())
                        myfile.write("\t"+self.response+"\n\n")

                if (self.counter == 500) and not self.half:
                    with open("flash_test_log.log", "a") as myfile:
                        myfile.write("\n\n-----------------------\n\n")
                    self.mFi.switchOFF(4)
                    self.chamber.set_values(temperature=45, oStart=True)
                    time.sleep(15*60)
                    time.sleep(2)
                    self.mFi.switchON(4)
                    self.half = True

                if (self.counter == 1000) and not self.all:
                    self.mFi.switchOFF(4)
                    self.chamber.set_values(temperature=45, oStart=False)
                    self.stop()
                    self.all = True
            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())

    def send(self, cmd):
        print u">", cmd
        self.qcmd.put(cmd)

    def clearQ(self):
        with self.qcmd.mutex:
            self.qcmd.queue.clear()

    def stop(self):
        self._work_flag = False
        self.serial.close()

if __name__ == "__main__":
    print u"Rozpoczecie testu", datetime.datetime.now()
    st = COM(115200, "/dev/ttyUSB1", 30)
    #st.start()
    st.run()

    #time.sleep(24*60*60)
