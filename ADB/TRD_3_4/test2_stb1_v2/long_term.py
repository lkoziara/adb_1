# -*- coding: utf-8 -*-


import serial
import Queue
from threading import Thread
import sys
import traceback
import time
import os
import datetime
import re

from sys import platform as _platform
from mFi import *
from WKL64 import *
from COM import *
from ADBlib import *


if __name__ == "__main__":

    Tcycle = 2*60
    USB_STB = "/dev/ttyUSB3"
    ETH_MFI = "192.168.0.2"
    MFI_SOCKET = 4

    filename = "trd_3_4.log"

    miFi = mFi(ETH_MFI, "ubnt", "ubnt")
    miFi.switchON(MFI_SOCKET)

    stb = COM(115200, USB_STB, 0, "com_log.log")
    stb.start()
    time.sleep(30)
    stb.send("transponder 0 802000 6875 q256")
    time.sleep(1)
    stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
    time.sleep(1)
    stb.send("video_decoder 3")
    time.sleep(1)
    stb.send("audio_decoder 2")
    time.sleep(1)
    stb.stop()

    with open(filename, "a") as myfile:
        myfile.write("TEST: TRD 3.4\n")
        myfile.write("\tLong Term Stability\n\n")

    # ## SET TEMP & GRADIENT

    TEST_SUCCESS = True
    PREV_TEST = True
    Decklink_OK = True
    stb = None
    COUNTER = 0
    ERRORS = 0

    for idx in xrange(7):

        START_TIMESTAMP = time.time()

        while True:
            if ((time.time() - START_TIMESTAMP) >= (90*60)+(180*60)+(180*60)): # (90*60)+(180*60)+(180*60)
                print "END", datetime.datetime.now()
                break

            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################
            try:
                TEST_SUCCESS = True
                Decklink_OK = True
                with open(filename, "a") as myfile:
                    myfile.write("\nTEST: %s %s\n" % (COUNTER, datetime.datetime.now()))
                print "TEST: ", COUNTER, datetime.datetime.now()

                stb = COM(115200, USB_STB, 3, "com_log_file.log")
                stb.start()

                # ######################## TATS 0 ########################
                if not check_tats(stb, filename):
                    TEST_SUCCESS = False
                # ######################## SET DHCP ######################
                if not setDHCPandPING(stb, filename):
                    TEST_SUCCESS = False
                # ######################## DECKLINK ######################
                time.sleep(Tcycle-12)
            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())

            finally:
                if stb:
                    stb.stop()

                if not PREV_TEST and not TEST_SUCCESS:
                    miFi.switchOFF(MFI_SOCKET)
                    time.sleep(2)
                    miFi.switchON(MFI_SOCKET)
                    stb = COM(115200, USB_STB, 0, "com_log.log")
                    stb.start()
                    time.sleep(30)
                    stb.send("transponder 0 802000 6875 q256")
                    time.sleep(1)
                    stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
                    time.sleep(1)
                    stb.send("video_decoder 3")
                    time.sleep(1)
                    stb.send("audio_decoder 2")
                    time.sleep(1)
                    stb.stop()
                    with open(filename, "a") as myfile:
                        myfile.write("\n\n")
                        myfile.write("-------- RESTART STB --------")
                        myfile.write("\n\tSend: transponder 0 802000 6875 q256")
                        myfile.write("\n\tSend: avp_pids_set 0x204 0x201 0x201")
                        myfile.write("\n\tSend: video_decoder 1")
                        myfile.write("\n\tSend: audio_decoder 2")

                if not TEST_SUCCESS:
                    ERRORS += 1

                PREV_TEST = TEST_SUCCESS
                COUNTER += 1

    ###########################################################################
    ##############################################
    ###########################################################################
    ##############################################

    with open(filename, "a") as myfile:
        myfile.write("\n\n----------Additionally------\n\n")

    START_TIMESTAMP = time.time()
    while True:

        if (time.time() - START_TIMESTAMP) >= (24*60*60)+(40*60):
            break

        try:
            TEST_SUCCESS = True
            Decklink_OK = True
            with open(filename, "a") as myfile:
                myfile.write("\nTEST: %s %s\n" % (COUNTER, datetime.datetime.now()))
            print "TEST: ", COUNTER, datetime.datetime.now()

            stb = COM(115200, USB_STB, 3, "com_log_file.log")
            stb.start()

            # ######################## TATS 0 ########################
            if not check_tats(stb, filename):
                TEST_SUCCESS = False
            # ######################## SET DHCP ######################
            if not setDHCPandPING(stb, filename):
                TEST_SUCCESS = False
            # ######################## DECKLINK ######################
            time.sleep(Tcycle-12)  # +4 s BMD

        except:
            with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
        finally:
            if stb:
                stb.stop()

            if not PREV_TEST and not TEST_SUCCESS:
                miFi.switchOFF(MFI_SOCKET)
                time.sleep(2)
                miFi.switchON(MFI_SOCKET)
                stb = COM(115200, USB_STB, 0, "com_log.log")
                stb.start()
                time.sleep(30)
                stb.send("transponder 0 802000 6875 q256")
                time.sleep(1)
                stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
                time.sleep(1)
                stb.send("video_decoder 3")
                time.sleep(1)
                stb.send("audio_decoder 2")
                time.sleep(1)
                stb.stop()
                with open(filename, "a") as myfile:
                    myfile.write("\n\n")
                    myfile.write("-------- RESTART STB --------")
                    myfile.write("\n\tSend: transponder 0 802000 6875 q256")
                    myfile.write("\n\tSend: avp_pids_set 0x204 0x201 0x201")
                    myfile.write("\n\tSend: video_decoder 1")
                    myfile.write("\n\tSend: audio_decoder 2")

            if not TEST_SUCCESS:
                ERRORS += 1

            PREV_TEST = TEST_SUCCESS
            COUNTER += 1

    with open(filename, "a") as myfile:
            myfile.write("\n\n-------------------------------------------\n\n")
            myfile.write("Cycle: %s\n" % COUNTER)
            myfile.write("ERRORS: %s" % ERRORS)

    time.sleep(5*60)
    miFi.stop()
