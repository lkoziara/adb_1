# -*- coding: utf-8 -*-


import serial
import Queue
from threading import Thread
import sys
import traceback
import time
import os
import datetime
import re

from sys import platform as _platform
from mFi import *
from WKL64 import *
from COM import *
from ADBlib import *
from KeySight34972A import *
from pyBMD import *


class VIDEO(Thread):
    def __init__(self, bmd=None, frames=10):
        Thread.__init__(self)
        self.bmd = None
        self.work_flag = True
        self.frames = frames
        self.valid = 0
        self.invalid = 0

    def callback_frame(self, width, height, bytes_in_row, pixel_format, buff_p):
        return True

    def callback_start(self):
        return True

    def callback_stop(self, infalidnb, validnb):
        self.valid += int(validnb)
        self.invalid += int(infalidnb)
        return True

    def run(self):
        self.bmd = BMD(0)
        self.bmd.setStartLoopCallback(self.callback_start)
        self.bmd.setStopLoopCallback(self.callback_stop)
        self.bmd.setFrameArrivedCallback(self.callback_frame)
        while self.work_flag:
            self.bmd.startLoop(self.frames)
            time.sleep(1)

    def reset_COUNTER(self):
        self.valid = 0
        self.invalid = 0

    def getInvalidFrames(self):
        return self.bmd.getInvalidFrames()

    def getValidFrames(self):
        return self.bmd.getValidFrames()

    def resetCOUNTERs(self):
        self.bmd.resetCOUNTERs()

    def get_COUNTER(self):
        return [self.invalid, self.valid]

    def stop(self):
        self.work_flag = False
        if self.bmd:
            self.bmd.stopLoop()
            self.bmd.delete()


if __name__ == "__main__":

    Tcycle = 2*60
    frame_threshold = 0.99
    USB_STB = "/dev/ttyUSB0"
    ETH_MFI = "192.168.0.2"
    USB_CHAMBER = "/dev/ttyUSB1"
    temp_USB = "/dev/usbtmc1"
    MFI_SOCKET = 4

    filename = "trd_3_4.log"

    chamber = WKL64(19200, USB_CHAMBER, bus=1)
    chamber.start()
    time.sleep(1)
    chamber.capture_logs()

    miFi = mFi(ETH_MFI, "ubnt", "ubnt")
    miFi.switchON(MFI_SOCKET)

    key = KeySight34972A(temp_USB)
    key.configTemperature(type="K", sockets=[102, 103, 104, 105, 106, 107, 108, 109, 110, 111])
    time.sleep(1)
    key.continuousMeasureTemperature(sockets=[102, 103, 104, 105, 106, 107, 108, 109, 110, 111])

    stb = COM(115200, USB_STB, 0, "com_log.log")
    stb.start()
    time.sleep(30)
    stb.send("transponder 0 802000 6875 q256")
    time.sleep(1)
    stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
    time.sleep(1)
    stb.send("video_decoder 3")
    time.sleep(1)
    stb.send("audio_decoder 2")
    time.sleep(1)
    stb.stop()

    with open(filename, "a") as myfile:
        myfile.write("TEST: TRD 3.4\n")
        myfile.write("\tLong Term Stability\n\n")

    # ## SET TEMP & GRADIENT

    TEST_SUCCESS = True
    PREV_TEST = True
    Decklink_OK = True
    stb = None
    COUNTER = 0
    ERRORS = 0

    video = VIDEO(frames=100)
    video.start()
    time.sleep(1)

    for idx in xrange(7):
        temp_flag = {
            "heating": False,
            "cooling": False,
            "3hour_after_heating": False,
            "3hour_after_cooling": False,
        }
        START_TIMESTAMP = time.time()

        while True:
            if ((time.time() - START_TIMESTAMP) >= (90*60)+(180*60)+(180*60)): # (90*60)+(180*60)+(180*60)
                # koniec 0st  3h
                print "END", datetime.datetime.now()
                break
            elif ((time.time() - START_TIMESTAMP) >= (90*60)+(180*60)) and not temp_flag["3hour_after_cooling"]:  # (90*60)+(180*60)
                # koniec 45st-->0st  45min
                # poczatek 0st  3h
                print "0st 3h", datetime.datetime.now()
                chamber.set_values(temperature=0, oStart=True)
                temp_flag["3hour_after_cooling"] = True
            elif ((time.time() - START_TIMESTAMP) >= (45*60)+(180*60)) and not temp_flag["cooling"]:  # (45*60)+(180*60)
                # koniec 3h 45st
                # poczatek  45st --> 0st  45min
                print "45st --  1st/min  --> 0st 45min", datetime.datetime.now()
                chamber.set_gradients(heating=1, cooling=1)
                chamber.set_values_gradient(temperature=0, oStart=True)
                temp_flag["cooling"] = True
            elif ((time.time() - START_TIMESTAMP) >= (45*60)) and not temp_flag["3hour_after_heating"]:  # (45*60) > 3h
                # konice 0st --> 45 st 45min
                # poczatek 45st 3h
                print "45st 3h", datetime.datetime.now()
                chamber.set_values(temperature=45, oStart=True)
                temp_flag["3hour_after_heating"] = True

            elif ((time.time() - START_TIMESTAMP) >= 0) and not temp_flag["heating"]:
                # poczatek 0st --> 45st 45min
                print "0st -- 1st/min --> 45st 45min", datetime.datetime.now()
                chamber.set_gradients(heating=1, cooling=1)
                chamber.set_values_gradient(temperature=45, oStart=True)
                temp_flag["heating"] = True

            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################
            try:
                TEST_SUCCESS = True
                Decklink_OK = True
                with open(filename, "a") as myfile:
                    myfile.write("\nTEST: %s %s\n" % (COUNTER, datetime.datetime.now()))
                print "TEST: ", COUNTER, datetime.datetime.now()

                stb = COM(115200, USB_STB, 3, "com_log_file.log")
                stb.start()

                video.reset_COUNTER()

                # ######################## TATS 0 ########################
                if not check_tats(stb, filename):
                    TEST_SUCCESS = False
                # ######################## SET DHCP ######################
                if not setDHCPandPING(stb, filename):
                    TEST_SUCCESS = False
                # ######################## DECKLINK ######################
                time.sleep(Tcycle-16)  # +4 s BMD

                print "\tVIDEO:"
                print "\t  valid:", video.valid
                print "\t  invalid:", video.invalid
                with open(filename, "a") as myfile:
                    myfile.write("\n\tVIDEO:")
                    myfile.write("\n\t  valid: %d" % video.valid)
                    myfile.write("\n\t  invalid: %d" % video.invalid)
                    try:
                        if (float(video.valid)/float(video.valid+video.invalid)) >= frame_threshold:
                            myfile.write("\n\tVIDEO - OK")
                            print "\tVIDEO - OK"
                        else:
                            TEST_SUCCESS = False
                            myfile.write("\n\tVIDEO - ERROR")
                            print "\tVIDEO - ERROR"
                    except:
                        with open("script_error.log", "a") as myfile:
                            myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                            myfile.write("\n%s\n" % traceback.format_exc())

                time.sleep(4)
            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())

            finally:
                if stb:
                    stb.stop()

                if not PREV_TEST and not TEST_SUCCESS:
                    miFi.switchOFF(MFI_SOCKET)
                    time.sleep(2)
                    miFi.switchON(MFI_SOCKET)
                    stb = COM(115200, USB_STB, 0, "com_log.log")
                    stb.start()
                    time.sleep(30)
                    stb.send("transponder 0 802000 6875 q256")
                    time.sleep(1)
                    stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
                    time.sleep(1)
                    stb.send("video_decoder 3")
                    time.sleep(1)
                    stb.send("audio_decoder 2")
                    time.sleep(1)
                    stb.stop()
                    with open(filename, "a") as myfile:
                        myfile.write("\n\n")
                        myfile.write("-------- RESTART STB --------")
                        myfile.write("\n\tSend: transponder 0 802000 6875 q256")
                        myfile.write("\n\tSend: avp_pids_set 0x204 0x201 0x201")
                        myfile.write("\n\tSend: video_decoder 1")
                        myfile.write("\n\tSend: audio_decoder 2")

                if not TEST_SUCCESS:
                    ERRORS += 1

                PREV_TEST = TEST_SUCCESS
                COUNTER += 1

    ###########################################################################
    ##############################################
    ###########################################################################
    ##############################################

    with open(filename, "a") as myfile:
        myfile.write("\n\n----------Additionally------\n\n")
    temp_flag = {
        "dw1": False,
        "tt": False,
        "dw2": False,
    }
    START_TIMESTAMP = time.time()
    while True:

        if (time.time() - START_TIMESTAMP) >= (24*60*60)+(40*60):
            break
        elif ((time.time() - START_TIMESTAMP) >= (12*60*60) + (40*60)) and not temp_flag["dw2"]:   #(12*60*60) + (40*60)
            print "45stn  12h", datetime.datetime.now()
            chamber.set_values(temperature=45, oStart=True)
            temp_flag["dw2"] = True

        elif ((time.time() - START_TIMESTAMP) >= (12*60*60)) and not temp_flag["tt"]:
            print "5st -->  45st  40min", datetime.datetime.now()
            chamber.set_gradients(heating=1, cooling=1)
            chamber.set_values_gradient(temperature=45, oStart=True)
            temp_flag["tt"] = True

        elif ((time.time() - START_TIMESTAMP) >= 0) and not temp_flag["dw1"]:
            print "5st  12h", datetime.datetime.now()
            chamber.set_values(temperature=5, oStart=True)
            temp_flag["dw1"] = True

        try:
            TEST_SUCCESS = True
            Decklink_OK = True
            with open(filename, "a") as myfile:
                myfile.write("\nTEST: %s %s\n" % (COUNTER, datetime.datetime.now()))
            print "TEST: ", COUNTER, datetime.datetime.now()

            stb = COM(115200, USB_STB, 3, "com_log_file.log")
            stb.start()
            video.reset_COUNTER()

            # ######################## TATS 0 ########################
            if not check_tats(stb, filename):
                TEST_SUCCESS = False
            # ######################## SET DHCP ######################
            if not setDHCPandPING(stb, filename):
                TEST_SUCCESS = False
            # ######################## DECKLINK ######################
            time.sleep(Tcycle-16)  # +4 s BMD

            print "\tVIDEO:"
            print "\t  valid:", video.valid
            print "\t  invalid:", video.invalid
            with open(filename, "a") as myfile:
                myfile.write("\n\tVIDEO:")
                myfile.write("\n\t  valid: %d" % video.valid)
                myfile.write("\n\t  invalid: %d" % video.invalid)
                try:
                    if (float(video.valid)/float(video.valid+video.invalid)) >= frame_threshold:
                        myfile.write("\n\tVIDEO - OK")
                        print "\tVIDEO - OK"
                    else:
                        TEST_SUCCESS = False
                        myfile.write("\n\tVIDEO - ERROR")
                        print "\tVIDEO - ERROR"
                except:
                    with open("script_error.log", "a") as myfile:
                        myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                        myfile.write("\n%s\n" % traceback.format_exc())

            time.sleep(4)

        except:
            with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
        finally:
            if stb:
                stb.stop()

            if not PREV_TEST and not TEST_SUCCESS:
                miFi.switchOFF(MFI_SOCKET)
                time.sleep(2)
                miFi.switchON(MFI_SOCKET)
                stb = COM(115200, USB_STB, 0, "com_log.log")
                stb.start()
                time.sleep(30)
                stb.send("transponder 0 802000 6875 q256")
                time.sleep(1)
                stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
                time.sleep(1)
                stb.send("video_decoder 3")
                time.sleep(1)
                stb.send("audio_decoder 2")
                time.sleep(1)
                stb.stop()
                with open(filename, "a") as myfile:
                    myfile.write("\n\n")
                    myfile.write("-------- RESTART STB --------")
                    myfile.write("\n\tSend: transponder 0 802000 6875 q256")
                    myfile.write("\n\tSend: avp_pids_set 0x204 0x201 0x201")
                    myfile.write("\n\tSend: video_decoder 1")
                    myfile.write("\n\tSend: audio_decoder 2")

            if not TEST_SUCCESS:
                ERRORS += 1

            PREV_TEST = TEST_SUCCESS
            COUNTER += 1

    with open(filename, "a") as myfile:
            myfile.write("\n\n-------------------------------------------\n\n")
            myfile.write("Cycle: %s\n" % COUNTER)
            myfile.write("ERRORS: %s" % ERRORS)

    key.stopContinousMeasure()
    chamber.stop_capturing_logs()
    chamber.set_values(temperature=23, oStart=True)
    video.stop()

    time.sleep(5*60)
    miFi.stop()
    chamber.set_values(temperature=20, oStart=False)
    chamber.stop()
