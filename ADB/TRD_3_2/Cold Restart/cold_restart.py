# -*- coding: utf-8 -*-
# PYTHONPATH="/home/cp/stb/lib"

import datetime
from COM import *
from chroma import *
from WKL64 import *
import os
from sys import platform as _platform
from pyBMD import *
from ADBlib import *
import sys

FRAME = {"valid": 0, "invalid": 0}


def callback_frame(width, height, bytes_in_row, pixel_format, buff_p):
    # print "dev_id: %d, w: %d, h: %d, inrow: %d" % (
        #    BMD_id, width, height, bytes_in_row)
    return True


def callback_start():
    return True


def callback_stop(infalidnb, validnb):
    print "\tInvalid: %s, Valid: %s" % (infalidnb, validnb)
    FRAME["valid"] = validnb
    FRAME["invalid"] = infalidnb
    return True


if __name__ == "__main__":
    USB_STB = "/dev/ttyUSB0"
    USB_CHROMA = "/dev/ttyUSB2"
    USB_CHAMBER = "/dev/ttyUSB1"
    frame_threshold = 0.99

    Ton = 60
    Termalization_time = 30*60  # 30min

    Tmin = 5
    Tmax = 40
    Vnom_min = 230
    Vnom_max = 230
    Fnom_min = 50
    Fnom_max = 50

    TESTS = [{
            "Ton": 60,
            "Toff": Termalization_time,
            "volts": Vnom_min - 0.1*Vnom_min,
            "frequency": Fnom_min-3,
            "logfile": "test 3.2 Cold Restart_A.log"},
        {
            "Ton": 60,
            "Toff": Termalization_time,
            "volts": Vnom_max + 0.1*Vnom_max,
            "frequency": Fnom_max+3,
            "logfile": "test 3.2 Cold Restart_B.log"},
        {
            "Ton": 60,
            "Toff": Termalization_time,
            "volts": 230,
            "frequency": 50,
            "logfile": "test 3.2 Cold Restart_C.log"}
    ]

    chroma = CHROMA(19200, USB_CHROMA, 1)
    chroma.start()
    chroma.send("OUTPut OFF")

    chamber = WKL64(19200, USB_CHAMBER, bus=1)
    chamber.start()
    time.sleep(1)
    chamber.capture_logs()

    BMD_id = 0
    bmd = BMD(BMD_id)
    bmd.setStartLoopCallback(callback_start)
    bmd.setStopLoopCallback(callback_stop)
    bmd.setFrameArrivedCallback(callback_frame)

    chamber.set_values(temperature=0, oStart=True)
    time.sleep(30*60)  # chamber termalization
    ErrorListStart = []
    ErrorListEnd = []
    for test in TESTS:
        with open(test["logfile"], "a") as myfile:
            myfile.write("TRD 3.2 Cold Restart\n")
            myfile.write("\ttemperature: 0st\n")
            myfile.write("\tAC: %sV\n" % test["volts"])
            myfile.write("\tFrequency: %sHz\n\n" % test["frequency"])

        chroma.send("VOLT:AC %s" % test["volts"])
        chroma.send("FREQ %s" % test["frequency"])
        stb = None
        TEST_SUCCESS = True
        Decklink_OK = True

        ERRORS = 0
        for idx in xrange(10):
            try:
                print "TEST %s %s" % (idx, datetime.datetime.now())
                with open(test["logfile"], "a") as myfile:
                    myfile.write("\nTEST %s %s" % (idx, datetime.datetime.now()))

                chroma.send("OUTPut ON")
                stb = COM(115200, USB_STB, 3, "com_log_file.log")
                stb.start()
                time.sleep(25)
                TEST_SUCCESS = True
                Decklink_OK = True

                stb.send("transponder 0 802000 6875 q256")
                time.sleep(1)
                stb.send("avp_pids_set 0x101 0x100 0x100")
                time.sleep(1)
                stb.send("video_decoder 3")
                time.sleep(1)
                stb.send("audio_decoder 2")
                time.sleep(3)
                with open(test["logfile"], "a") as myfile:
                    myfile.write("\n\tRESTART STB")
                    myfile.write("\n\tSend: transponder 0 802000 6875 q256")
                    myfile.write("\n\tSend: avp_pids_set 0x204 0x201 0x201")
                    myfile.write("\n\tSend: video_decoder 1")
                    myfile.write("\n\tSend: audio_decoder 2")
                    myfile.write("\n\tSend: tats 0\n")

                try:
                    response = urllib2.urlopen('http://192.168.0.6:6446/Monitor/MonitorService.svc/Racks/local/1/reports/1/alarms', timeout=3)
                    ErrorListStart = json.load(response)
                except:
                    ErrorListStart = None
                    with open("script_error.log", "a") as myfile:
                        myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                        myfile.write("\n%s\n" % traceback.format_exc())

                # ######################## TATS 0 ########################
                if not check_tats(stb, test["logfile"]):
                    TEST_SUCCESS = False
                # ######################## SET DHCP ######################
                if not setDHCPandPING(stb, test["logfile"]):
                    TEST_SUCCESS = False
                # ######################## DECKLINK ######################

                bmd.startLoop(100)
                if FRAME["invalid"] == 1:
                    FRAME["invalid"] = FRAME["invalid"]-1
                    FRAME["valid"] += 1
                with open(test["logfile"], "a") as myfile:
                    myfile.write("\n\n\tValid: frames: %d" % (FRAME["valid"]))
                    myfile.write("\n\tInvalid: frames: %d" % (FRAME["invalid"]))
                    myfile.write("\n\tVALID/ALL: %s" % float(((FRAME["valid"]+1)/(FRAME["valid"]+FRAME["invalid"]))))

                try:
                    if float(((FRAME["valid"])/(FRAME["valid"]+FRAME["invalid"]))) >= frame_threshold:
                        Decklink_OK = True
                    else:
                        Decklink_OK = False
                        TEST_SUCCESS = False
                except:
                    with open("script_error.log", "a") as myfile:
                        myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                        myfile.write("\n%s\n" % traceback.format_exc())

                time.sleep(test["Ton"] - 56)

                try:
                    response = urllib2.urlopen('http://192.168.0.6:6446/Monitor/MonitorService.svc/Racks/local/1/reports/1/alarms', timeout=3)
                    ErrorListEnd = json.load(response)
                except:
                    ErrorListEnd = None
                    with open("script_error.log", "a") as myfile:
                        myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                        myfile.write("\n%s\n" % traceback.format_exc())
                time.sleep(4)
            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
            finally:
                chroma.send("OUTPut OFF")

                rrResult = RRsystem(ErrorListStart, ErrorListEnd, test["logfile"])
                if (rrResult is True) and (Decklink_OK is True):
                    print "\tVIDEO - OK"
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\tVIDEO - OK")
                else:
                    TEST_SUCCESS = False
                    print "\tVIDEO - ERROR"
                    with open(test["logfile"], "a") as myfile:
                        myfile.write("\n\tVIDEO - ERROR")

                if stb:
                    stb.stop()
                if not TEST_SUCCESS:
                    ERRORS += 1
                time.sleep(Termalization_time)

        with open(test["logfile"], "a") as myfile:
            myfile.write("\n\n-------------------------------------------\n\n")
            myfile.write("Errors: %s" % ERRORS)
    chroma.stop()
    chamber.stop_capturing_logs()
    chamber.stop()