# -*- coding: utf-8 -*-
import datetime
import time
import json
from COM import *
from mFi import *
from WKL64 import *
from ADBlib import *
from pyBMD import *

FRAME = {"valid": 0, "invalid": 0}


def callback_frame(width, height, bytes_in_row, pixel_format, buff_p):
    # print "dev_id: %d, w: %d, h: %d, inrow: %d" % (
        #    BMD_id, width, height, bytes_in_row)
    return True


def callback_start():
    return True


def callback_stop(infalidnb, validnb):
    print "\tInvalid: %s, Valid: %s" % (infalidnb, validnb)
    FRAME["valid"] = validnb
    FRAME["invalid"] = infalidnb
    return True

if __name__ == "__main__":
    Tcycle = 60
    USB_STB = "/dev/ttyUSB0"
    ETH_MFI = "192.168.0.2"
    USB_CHAMBER = "/dev/ttyUSB1"
    MFI_SOCKET = 4
    filename = "absolute_max_temperature_logs.log"
    frame_threshold = 0.99
    #  2h -5 st

    mFi = mFi(ETH_MFI, "ubnt", "ubnt")

    chamber = WKL64(19200, USB_CHAMBER, bus=1)
    chamber.start()
    time.sleep(2)
    chamber.capture_logs()
    chamber.set_values(temperature=25, oStart=True)

    PREV_TEST = True
    TEST_SUCCESS = True
    stb = None
    ErrorListStart = []
    ErrorListEnd = []
    CYCLES = 0
    ERRORS = 0
    START_TIMESTAMP = time.time()
    Decklink_OK = True

    stage1 = False
    stage2 = False
    stage3 = False
    stage4 = False

    mFi.switchON(MFI_SOCKET)
    stb = COM(115200, USB_STB, 0, "com_log.log")
    stb.start()
    time.sleep(30)
    stb.send("transponder 0 802000 6875 q256")
    time.sleep(1)
    stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
    time.sleep(1)
    stb.send("video_decoder 3")
    time.sleep(1)
    stb.send("audio_decoder 2")
    stb.stop()

    BMD_id = 0
    bmd = BMD(BMD_id)
    bmd.setStartLoopCallback(callback_start)
    bmd.setStopLoopCallback(callback_stop)
    bmd.setFrameArrivedCallback(callback_frame)

    while True:
        if (time.time() - START_TIMESTAMP) >= (14*60*60 + 20*60):  # 24h
            break

        if (time.time() - START_TIMESTAMP) >= (12*60*60 + 20*60)and (not stage4):
            chamber.set_gradients(heating=0.5, cooling=0.5)
            chamber.set_values_gradient(temperature=25, oStart=True)
            stage4 = True
            print "stage4"

        if (time.time() - START_TIMESTAMP) >= (10*60*60 + 70*60) and (not stage3): #(10*60*60 + 70*60)
            chamber.set_gradients(heating=0.5, cooling=0.5)
            chamber.set_values_gradient(temperature=25, oStart=True)
            stage3 = True
            print "stage3"

        if (time.time() - START_TIMESTAMP) >= 70*60 and (not stage2):  # after 70 min
            chamber.set_values(temperature=60, oStart=True)
            stage2 = True
            print "stage2"

        if (time.time() - START_TIMESTAMP) >= 0 and (not stage1):
            chamber.set_gradients(heating=0.5, cooling=0.5)
            chamber.set_values_gradient(temperature=60, oStart=True)
            stage1 = True
            print "stage1"

        try:
            Decklink_OK = True
            TEST_SUCCESS = True
            print "TEST %s %s" % (CYCLES, datetime.datetime.now())
            with open(filename, "a") as myfile:
                myfile.write("\nTEST %s %s\n" % (CYCLES, datetime.datetime.now()))

            stb = COM(115200, USB_STB, 0, "com_log.log")
            stb.start()
            '''
            try:
                response = urllib2.urlopen('http://192.168.0.6:6446/Monitor/MonitorService.svc/Racks/local/1/reports/1/alarms', timeout=3)
                ErrorListStart = json.load(response)
            except:
                ErrorListStart = None
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
            '''
            # ######################## TATS 0 ########################
            if not check_tats(stb, filename):
                TEST_SUCCESS = False
            print "check_tats", TEST_SUCCESS
            # ######################## SET DHCP ######################
            if not setDHCPandPING(stb, filename):
                TEST_SUCCESS = False
            # ######################## DECKLINK ######################
            print "setDHCPandPING", TEST_SUCCESS
            bmd.startLoop(100)
            if FRAME["invalid"] == 1:
                FRAME["invalid"] = FRAME["invalid"]-1
                FRAME["valid"] += 1
            with open(filename, "a") as myfile:
                myfile.write("\n\n\tValid: frames: %d" % (FRAME["valid"]))
                myfile.write("\n\tInvalid: frames: %d" % (FRAME["invalid"]))
                myfile.write("\n\tVALID/ALL: %s" % float(((FRAME["valid"]+1)/(FRAME["valid"]+FRAME["invalid"]))))

            try:
                if float(((FRAME["valid"])/(FRAME["valid"]+FRAME["invalid"]))) >= frame_threshold:
                    Decklink_OK = True
                else:
                    Decklink_OK = False
                    TEST_SUCCESS = False
            except:
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())

            time.sleep(Tcycle-24)
            '''
            try:
                response = urllib2.urlopen('http://192.168.0.6:6446/Monitor/MonitorService.svc/Racks/local/1/reports/1/alarms', timeout=3)
                ErrorListEnd = json.load(response)
            except:
                ErrorListEnd = None
                with open("script_error.log", "a") as myfile:
                    myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                    myfile.write("\n%s\n" % traceback.format_exc())
            '''

        except:
            with open("script_error.log", "a") as myfile:
                myfile.write("\nSCRIPT ERROR%s" % datetime.datetime.now())
                myfile.write("\n%s\n" % traceback.format_exc())
        finally:
            #mFi.switchOFF(MFI_SOCKET)
            if stb:
                stb.stop()
            '''
            rrResult = RRsystem(ErrorListStart, ErrorListEnd, filename)
            if (rrResult is True) and (Decklink_OK is True):
                print "\tVIDEO - OK"
                with open(filename, "a") as myfile:
                    myfile.write("\n\tVIDEO - OK")
            else:
                TEST_SUCCESS = False
                print "\tVIDEO - ERROR"
                with open(filename, "a") as myfile:
                    myfile.write("\n\tVIDEO - ERROR")
            '''
            if Decklink_OK:
                print "\tVIDEO - OK"
                with open(filename, "a") as myfile:
                    myfile.write("\n\tVIDEO - OK")
            else:
                TEST_SUCCESS = False
                print "\tVIDEO - ERROR"
                with open(filename, "a") as myfile:
                    myfile.write("\n\tVIDEO - ERROR")

            print "Decklink_OK", TEST_SUCCESS

            if not TEST_SUCCESS and not PREV_TEST:
                mFi.switchOFF(MFI_SOCKET)
                time.sleep(2)
                mFi.switchON(MFI_SOCKET)
                time.sleep(30)
                stb = COM(115200, USB_STB, 0, "com_log.log")
                stb.start()
                stb.send("transponder 0 802000 6875 q256")
                time.sleep(1)
                stb.send("avp_pids_set 0x101 0x100 0x100")   # avp_pids_set 0x204 0x201 0x201   257
                time.sleep(1)
                stb.send("video_decoder 3")
                time.sleep(1)
                stb.send("audio_decoder 2")
                stb.stop()

            if not TEST_SUCCESS:
                ERRORS += 1
            CYCLES += 1
            PREV_TEST = TEST_SUCCESS

    with open(filename, "a") as myfile:
        myfile.write("\n\n---------------------------------------\n\n")
        myfile.write("Cycle: %s\n" % CYCLES)
        myfile.write("Errors: %s" % ERRORS)
    chamber.stop_capturing_logs()
    chamber.set_values(temperature=25, oStart=False)
    chamber.stop()
